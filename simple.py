import pygame


class Colors:
    "Colors"
    white = ((255, 255, 255))
    blue = ((0, 0, 255))
    green = ((0, 255, 0))
    red = ((255, 0, 0))
    black = ((0, 0, 0))
    orange = ((255, 100, 10))
    yellow = ((255, 255, 0))
    blue_green = ((0, 255, 170))
    marroon = ((115, 0, 0))
    lime = ((180, 255, 100))
    pink = ((255, 100, 180))
    purple = ((240, 0, 255))
    gray = ((127, 127, 127))
    magenta = ((255, 0, 230))
    brown = ((100, 40, 0))
    forest_green = ((0, 50, 0))
    navy_blue = ((0, 0, 100))
    rust = ((210, 150, 75))
    dandilion_yellow = ((255, 200, 0))
    highlighter = ((255, 255, 100))
    sky_blue = ((0, 255, 255))
    light_gray = ((200, 200, 200))
    dark_gray = ((50, 50, 50))
    tan = ((230, 220, 170))
    coffee_brown = ((200, 190, 140))
    moon_glow = ((235, 245, 255))


class View:
    "basic graphics info"

    def __init__(self):
        self.basic = 640
        self.size = self.width, self.height = self.basic, self.basic
        self.background_colour = Colors.white
        self.font0 = pygame.font.SysFont('Ariel', 18)
        self.screen = pygame.display.set_mode(self.size)
        self.screen.fill(self.background_colour)


class Process:
    "parts of the table"

    def __init__(self, view, algo):
        self.view = view
        self.algo = algo
        self.blocks = 10
        self.one = self.view.basic / (self.blocks + 5)
        self.offset = 1
        self.starting = self.one * self.offset
        self.starting_point = (self.starting, self.starting)
        self.top_header = self.header()
        self.left_col = self.column()
        self.series = algo(self)
        self.text = self.view.font0.render("X", 1, Colors.orange)

    def basic_row(self, orientation):
        for x in range(self.blocks):
            longer = int((self.starting + self.one) + (x * self.one))
            shorter = int(self.starting)
            x1 = None
            y1 = None
            color = Colors.green
            text = str(x + 1)
            if orientation > 0:
                color = Colors.orange
                x1 = longer
                y1 = shorter
            else:
                color = Colors.red
                x1 = shorter
                y1 = longer
            yield Label(self.view,
                        text=text,
                        x=x1,
                        y=y1,
                        color=color)

    def header(self):
        return list(self.basic_row(1))

    def column(self):
        return list(self.basic_row(-1))

    def render(self, screen):
        screen.blit(self.text, self.starting_point)
        for l1 in (
                self.top_header,
                self.left_col,
                self.series
        ):
            for x in l1:
                x.render(screen)

        pygame.display.flip()


class Label:
    def __init__(self, view, text, x, y, color):
        self.view = view
        self.img = self.view.font0.render(text, 1, color)
        self.x = x
        self.y = y

    def render(self, screen):
        screen.blit(self.img, (self.x, self.y))


class Game:
    def __init__(self, view, process):
        self.view = view
        self.process = process

    def render(self):
        self.process.render(self.view.screen)

    def run(self):
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                self.render()


def modulo_series(self):
    max1 = (self.blocks) * (self.blocks)

    for i in range(max1):
        y, x = divmod(i, self.blocks)

        x1 = int((self.starting + self.one) + (x * self.one))
        y1 = int((self.starting + self.one) + (y * self.one))

        text = str(i + 1)  # + "=" + str(x+1) + "," + str(y+1)
        yield Label(
            self.view,
            text=text,
            x=x1, y=y1,
            color=Colors.green
        )


# construct
pygame.init()
v = View()
p = Process(v, modulo_series)
g = Game(v, p)
g.run()
